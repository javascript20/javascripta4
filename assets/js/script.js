let foods = [
	[
		'egg sandwich',
		'tuna sandwich',
		'ham and bacon sandwich'
	],
	[
		'tinola',
		'sinigang',
		'sinampalukan',
		'paksiw'
	],
	[
		'unli wings',
		'samgyupsal',
		'milktea'
	]
];

foods.flat().forEach((food) => {
	console.log(food);
});

// Activity (15 mins)
// display all items inside the foods array
// egg sandwich
// tuna
// sandwich
// ham and bacon sandwich
// tinola
// sinigang
// sinampalukan
// paksiw
// ...
